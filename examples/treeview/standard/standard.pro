requires(qtHaveModule(widgets))

TEMPLATE = app

QT += gui qml quick widgets

RESOURCES += main.qml

SOURCES += \
    main.cpp \

target.path = $$[QT_INSTALL_EXAMPLES]/treeview/standard
INSTALLS += target
