/****************************************************************************
**
** Copyright (C) 2020 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of TreeView.
**
** $QT_BEGIN_LICENSE:GPL-MARKETPLACE-QT$
**
** Marketplace License Usage
** Users, who have licensed the Software under the Qt Marketplace license
** agreement, may use this file in accordance with the Qt Marketplace license
** agreement provided with the Software or, alternatively, in accordance with
** the terms contained in a written agreement between the licensee and The Qt
** Company. For licensing terms and conditions see
** https://www.qt.io/terms-conditions/#marketplace and
** https://www.qt.io/terms-conditions. For further information use the contact
** form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.15
import QtQuick.Window 2.1
import QtQuick.TreeView 2.15
import QtQuick.Controls 2.15
import Qt.labs.qmlmodels 1.0

Window {
    id: root
    width: 800
    height: 600
    visible: true
    visibility: Window.AutomaticVisibility

    Rectangle {
        anchors.fill: parent
        anchors.margins: 20
        border.width: 1

        CustomTreeView {
            id: treeView
            anchors.fill:parent
            anchors.margins: 1
            model: fileSystemModel
            clip: true
            focus: true
            navigationMode: CustomTreeView.List

            // Install pointer handlers to override the
            // default actions taken upon mouse press
            TapHandler {
                acceptedButtons: Qt.LeftButton
                onTapped: {
                    var posInTreeView = treeView.mapFromItem(parent, point.position)
                    var row = treeView.rowAtY(posInTreeView.y, true)
                    treeView.currentIndex = treeView.viewIndex(0, row);
                    if (tapCount == 2)
                        treeView.toggleExpanded(row)
                }
            }

            TapHandler {
                acceptedButtons: Qt.RightButton
                onTapped: {
                    var posInTreeView = treeView.mapFromItem(parent, point.position)
                    var row = treeView.rowAtY(posInTreeView.y, true)
                    treeView.currentIndex = treeView.viewIndex(0, row);
                    contextMenu.modelIndex = treeView.mapToModel(treeView.currentIndex)
                    contextMenu.popup()
                }
            }

            Menu {
                id: contextMenu
                property var modelIndex: treeView.model.index(-1, -1)
                property string name: modelIndex.valid ? treeView.model.data(modelIndex, treeView.textRole) : ""
                modal: true
                MenuItem { text: "Open " + contextMenu.name }
                MenuItem { text: "Rename "  + contextMenu.name }
                MenuItem { text: "Delete " + contextMenu.name }
            }
        }
    }
}
